package P12_8;

/**
 * Created by sethf_000 on 11/7/2015.
 *
 * This class sets the product info
 */
public class Product {
    private String name;
    private double price;
    private int number;
    private int count;

    public Product(int number, String name, double price, int count){
        this.number = number;
        this.name = name;
        this.price = price;
        this.count = count;
    }

    public int getNumber(){
        return this.number;
    }

    public String getName(){
        return this.name;
    }

    public double getPrice(){
        return this.price;
    }

    public int getCount(){
        return this.count;
    }



}
