package P12_8;

import java.util.Scanner;

/**
 * Created by sethf_000 on 11/7/2015.
 *
 * This class creates a VendingMachine object and fills it with products.
 * The user is prompted to identify as a user (buyer) or operator of the machine.
 * The user can buy products by entering coin amounts. If they do not enter enough money
 * their money is returned and they get no product. If the product is out, they are informed of that
 * and nothing is vended. Otehrwise, the product is vended. No change is returned for overpayment.
 * The operator can add new products or remove all the money in the machine.
 */
public class Driver {

    //create vending machine object
    private static VendingMachine vm = new VendingMachine();
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        //add new products to the machine (products take a product number, name, price, and count of items)
        vm.addProduct(1, "Snickers", 1.50, 5);
        vm.addProduct(2, "Hershey's", 1.50, 3);
        vm.addProduct(3, "Milky Way", 1.50, 0);
        vm.addProduct(4, "Chips", 1.00, 4);
        vm.addProduct(5, "Crackers", 2.00, 3);
        vm.addProduct(6, "Pretzels", 2.00, 5);

        System.out.println("***WELCOME TO THE VENDING MACHINE!***\n");
        int entry = 0;

        //start main program loop. it keeps running as long as the user doesn't enter 3
        while (entry != 3){
            System.out.println("\nMENU: 1)User 2)Operator 3)Quit");
            System.out.print("Enter 1, 2, or 3: ");
            //check for valid input
            if (!scan.hasNextInt()){
                do {
                    System.out.println("That is not a valid entry. Please select from the menu.");
                    scan.nextLine();
                }while(!scan.hasNextInt());
            }
            entry = scan.nextInt();
            //calls the "user" method
            if (entry == 1){
                userProgram();
            }
            //calls the "operator" method
            else if (entry == 2){
                operatorProgram();
            }
            //exits loop
            else {
                entry = 3;
            }
        }

        System.out.println("\nTHANKS, COME AGAIN!");

    }

    //called if 1 is entered from menu
    public static void userProgram(){
        System.out.println("\nSelect a product from the List: ");
        //displays all products
        vm.getProducts();
        //check for valid entry
        if (!scan.hasNextInt()){
            do {
                scan.nextLine();
                System.out.println("That is not a valid entry. Please select from the menu.");

            }while(!scan.hasNextInt());
        }
        int choice = scan.nextInt();

        double payment = 0;
        int coin = 0;
        //allow for the selection of coin amounts. "4" lets the user submit total amount
        System.out.println("Select the coins you are entering: ");
        System.out.println("1. Dollar ($1.00), 2. Quarter ($.25), 3. Dime ($.10) 4. Submit");
        System.out.println("Enter 1,2 or 3 to enter that value.\n Enter '4' to submit your payment");

        //continues as long as user does not enter 4
        while (coin != 4){
            if (!scan.hasNextInt()){
                do {
                    scan.nextLine();
                    System.out.println("That is not a valid entry. Please enter 1-4.");

                }while(!scan.hasNextInt());
            }
            coin = scan.nextInt();
            //add coin amounts to total payment amount
            if (coin == 1){
                payment += 1;
            }
            else if (coin == 2){
                payment += .25;
            }
            else if (coin == 3){
                payment += .1;
            }
            System.out.println("Current Payment: " + payment);
        }

        //check payment amount vs. product price
        double change = vm.checkProduct(choice, payment);

        //if not enough money was submitted, it is returned
        if (change > 0){
            System.out.println("Here is your money back: " + change);
        }

    }

    //when 2 is selected from main menu
    public static void operatorProgram(){

        System.out.println("Hello, Operator.");
        System.out.println("Enter 1) Remove money or 2) Add a product ");
        if (!scan.hasNextInt()){
            do {
                scan.nextLine();
                System.out.println("That is not a valid entry. Please select from the menu.");

            }while(!scan.hasNextInt());
        }
        int choice = scan.nextInt();
        //if operator selects to remove money
        if (choice == 1){
            //returns all money in the machine
            double money = vm.getTotalMoney();
            System.out.println("Here is all the money: $" + money);
        }
        //if operator wants to add product
        else if (choice == 2){
            //get all information for the product
            System.out.println("You will enter a name, a price, and how many you are adding.");

            System.out.print("Enter product name: ");
            String name = scan.next();

            System.out.print("Enter a price: ");
            double price = scan.nextDouble();

            System.out.print("Enter the number you are adding: ");
            int count = scan.nextInt();

            //adds new product
            int prodNo = vm.getProductsCount() + 1;
            vm.addProduct(prodNo, name, price, count);
        }
    }



}

/*
Write a program that simulates a vending machine. Products can be purchased by
inserting coins with a value at least equal to the cost of the product. A user selects a
product from a list of available products, adds coins, and either gets the product or
gets the coins returned. The coins are returned if insufficient money was supplied
or if the product is sold out. The machine does not give change if too much money
was added. Products can be restocked and money removed by an operator. Follow
the design process that was described in this chapter. Your solution should include a
class VendingMachine that is not coupled with the Scanner or PrintStream classes.
 */