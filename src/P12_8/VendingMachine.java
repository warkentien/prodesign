package P12_8;

import java.util.ArrayList;

/**
 * Created by sethf_000 on 11/7/2015.
 *
 * Allows products to be added to the machine and keeps track of total money accepted
 * by the machine. It also computes whether or not enough money was entered by the user.
 */
public class VendingMachine {
    private double payment;
    //create list for products
    private ArrayList<Product> products = new ArrayList<Product>();
    private double totalMoney;

    public VendingMachine(){
        payment = 0;
    }

    //add new product
    public void addProduct(int number, String name, double price, int count){
        products.add(new Product(number, name, price, count));
    }

    //return product list
    public void getProducts(){
        for(Product product: products){
            System.out.println(product.getNumber() + ". " + product.getName() + ": $" + product.getPrice());
        }
    }

    //return all money in machine
    public double getTotalMoney(){
        double money = totalMoney;
        totalMoney = 0;
        return money;
    }

    //return number of products in list
    public int getProductsCount(){
        return products.size();
    }

    //checks if enough money was entered to purchase product, also checks
    //if there are enough of those products. returns payment if it's too small
    public double checkProduct(int number, double payment){
        this.payment = payment;
        int count = 1;
        //loop through products to get the one selected
        for(Product product: products){
            if (count == number){
                if (payment >= product.getPrice() && product.getCount() > 0){
                    System.out.println("\n***Vending: " + product.getName() + " ***");
                    this.totalMoney += this.payment;
                    this.payment = 0;
                }
                else if (payment < product.getPrice()){
                    System.out.println("You did not enter enough money.");
                    return payment;
                }
                else {
                    System.out.println("\n***THAT PRODUCT IS SOLD OUT***");
                }
            }
            count++;
        }
        return 0;
    }
}
