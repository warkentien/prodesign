package P12_6;

import java.util.Scanner;

/**
 * Created by sethf_000 on 11/6/2015.
 *
 * This program creates a three-level arithmetic game where you have to get 5 answers correct
 * before you can move on to the next level.
 * It uses the Player class to keep track of how many questions the player has gotten right and wrong
 * and the QuestionGenerator class to create new math questions and check their answers on the fly.
 */
public class Driver {

    //set global variables to be used in separate methods
    private static Player player = new Player();
    private static QuestionGenerator qG = new QuestionGenerator();
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {

        //display instructions
        System.out.println("Welcome to the Arithmetic Program!\n");
        System.out.println("RULES: There are three levels of questions you must complete.\nOnce you " +
                "get 5 answers correct you can move on to the next level. \nHowever, if you miss more " +
                "than two questions in one round, \nyou have to start over at the beginning of that round." +
                "\nLet's get started!\n");

        //create  boolean to mark if a level has been completed or not
        boolean pass = false;

        //stay in level one until 5 correct answers are entered. Two wrong answers on the same
        //question starts the level over again
        do {
            //enter method for level one
            pass = levelOne();

            //if two wrong answers are entered
            if (!pass){
                System.out.println("You did not pass this level, you have to start over.");
                player.resetCorrect();
                player.resetIncorrect();
            }
        }while (!pass);

        //indicate moving on to next level and reset answer counts
        System.out.println("***YOU PASSED LEVEL 1!***\n");
        player.resetCorrect();
        player.resetIncorrect();

        //stay in level two until 5 correct answers are entered. Two wrong answers on the same
        //question starts the level over again
        do {
            //enter method for level two
            pass = levelTwo();

            //if two wrong answers are entered
            if (!pass){
                System.out.println("You did not pass this level, you have to start over.");
                player.resetCorrect();
                player.resetIncorrect();
            }
        }while (!pass);

        //indicate moving on to next level and reset answer counts
        System.out.println("***YOU PASSED LEVEL 2!***\n");
        player.resetCorrect();
        player.resetIncorrect();

        //stay in level three until 5 correct answers are entered. Two wrong answers on the same
        //question starts the level over again
        do {
            //enter method for level three
            pass = levelThree();

            //if two wrong answers are entered
            if (!pass){
                System.out.println("You did not pass this level, you have to start over.");
                player.resetCorrect();
                player.resetIncorrect();
            }
        }while (!pass);

        //if all three levels are completed
        System.out.println("***YOU COMPLETED THE PROGRAM!***\n");
        System.out.println("***NOW YOU KNOW MATH!***");


    }

    //method for level one
    public static boolean levelOne(){
        int answer;
        System.out.println("Level 1: ");
        do {
            player.resetIncorrect();
            //print the question
            System.out.print(qG.getLevelOneQuestion());
            //check that a number was entered
            if (!scan.hasNextInt()){
                do {
                    scan.nextLine();
                    System.out.println("That is not a valid entry. Please enter a number.");

                }while(!scan.hasNextInt());
            }
            answer = scan.nextInt();
            if(answer == qG.getAnswer()){
                System.out.println("Correct");
                player.addCorrect();
            }
            else {
                player.addIncorrect();
                System.out.print("Incorrect. Try Again: ");
                //check that a number was entered
                if (!scan.hasNextInt()){
                    do {
                        scan.nextLine();
                        System.out.println("That is not a valid entry. Please enter a number.");

                    }while(!scan.hasNextInt());
                }
                answer = scan.nextInt();
                //check entered answer against actual answer
                if(answer == qG.getAnswer()){
                    System.out.println("Correct");
                    player.addCorrect();
                }
                else {
                    player.addIncorrect();
                }
            }
            System.out.println("Total Correct: " + player.getCorrect());
            System.out.println();
        }while (player.getCorrect() < 5 && player.getIncorrect() < 2);
        if(player.getIncorrect() == 2){
            return false;
        }
        else {
            return true;
        }
    }

    //method for level two
    public static boolean levelTwo(){
        int answer;
        System.out.println("Level 2: ");
        do {
            player.resetIncorrect();
            //print the question
            System.out.print(qG.getLevelTwoQuestion());
            //check that a number was entered
            if (!scan.hasNextInt()){
                do {
                    scan.nextLine();
                    System.out.println("That is not a valid entry. Please enter a number.");

                }while(!scan.hasNextInt());
            }
            answer = scan.nextInt();
            if(answer == qG.getAnswer()){
                System.out.println("Correct");
                player.addCorrect();
            }
            else {
                player.addIncorrect();
                System.out.print("Incorrect. Try Again: ");
                //check that a number was entered
                if (!scan.hasNextInt()){
                    do {
                        scan.nextLine();
                        System.out.println("That is not a valid entry. Please enter a number.");

                    }while(!scan.hasNextInt());
                }
                answer = scan.nextInt();

                //check entered answer against actual answer
                if(answer == qG.getAnswer()){
                    System.out.println("Correct");
                    player.addCorrect();
                }
                else {
                    player.addIncorrect();
                }
            }
            System.out.println("Total Correct: " + player.getCorrect());
            System.out.println();
        }while (player.getCorrect() < 5 && player.getIncorrect() < 2);
        if(player.getIncorrect() == 2){
            return false;
        }
        else {
            return true;
        }
    }

    //method for level three
    public static boolean levelThree(){
        int answer;
        System.out.println("Level 3: ");
        do {
            player.resetIncorrect();
            //print the question
            System.out.print(qG.getLevelThreeQuestion());
            //check that a number was entered
            if (!scan.hasNextInt()){
                do {
                    scan.nextLine();
                    System.out.println("That is not a valid entry. Please enter a number.");

                }while(!scan.hasNextInt());
            }
            answer = scan.nextInt();
            if(answer == qG.getAnswer()){
                System.out.println("Correct");
                player.addCorrect();
            }
            else {
                player.addIncorrect();
                System.out.print("Incorrect. Try Again: ");
                //check that a number was entered
                if (!scan.hasNextInt()){
                    do {
                        scan.nextLine();
                        System.out.println("That is not a valid entry. Please enter a number.");

                    }while(!scan.hasNextInt());
                }
                answer = scan.nextInt();

                //check entered answer against actual answer
                if(answer == qG.getAnswer()){
                    System.out.println("Correct");
                    player.addCorrect();
                }
                else {
                    player.addIncorrect();
                }
            }
            System.out.println("Total Correct: " + player.getCorrect());
            System.out.println();
        }while (player.getCorrect() < 5 && player.getIncorrect() < 2);
        if(player.getIncorrect() == 2){
            return false;
        }
        else {
            return true;
        }
    }



}



/*
Write a program that teaches arithmetic to a young child. The program tests addition
and subtraction. In level 1, it tests only addition of numbers less than 10 whose sum
is less than 10. In level 2, it tests addition of arbitrary one-digit numbers. In level 3, it
tests subtraction of one-digit numbers with a nonnegative difference.
Generate random problems and get the player�s input. The player gets up to two
tries per problem. Advance from one level to the next when the player has achieved a
score of five points.
 */
