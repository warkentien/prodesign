package P12_6;

import java.util.Random;

/**
 * Created by sethf_000 on 11/6/2015.
 */
public class QuestionGenerator {

    private int first;
    private int second;
    private int answer;
    private int diff;
    private Random rand = new Random();

    public QuestionGenerator(){

    }

    //returns level one question
    public String getLevelOneQuestion(){

        first = rand.nextInt(9) + 1;
        diff = 9 - first;
        second = rand.nextInt(diff + 1);
        answer = first + second;
        return "What is " + first + " + " + second + " : ";
    }

    //returns level two question
    public String getLevelTwoQuestion(){

        first = rand.nextInt(9) + 1;
        second = rand.nextInt(9) + 1;
        answer = first + second;
        return "What is " + first + " + " + second + " : ";
    }

    //returns level three question
    public String getLevelThreeQuestion(){

        first = rand.nextInt(9) + 1;
        second = rand.nextInt(first) + 1;
        answer = first - second;
        return "What is " + first + " - " + second + " : ";
    }

    //returns answer to the current question
    public int getAnswer(){
        return this.answer;
    }
}
