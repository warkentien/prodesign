package P12_6;

/**
 * Created by sethf_000 on 11/6/2015.
 */
public class Player {
    private String name;
    private int correct;
    private int incorrect;

    /*
    Sets Player's name on new object creation
     */
    public Player(){
    }

    /*
    Adds to Player's score on a correct answer
     */
    public void resetCorrect(){
        correct = 0;
    }

    public void resetIncorrect(){
        incorrect = 0;
    }

    public void addCorrect(){
        correct++;
    }

    public void addIncorrect(){
        incorrect++;
    }

    /*
    Return's Player's current score
     */
    public int getCorrect(){
        return this.correct;
    }

    public int getIncorrect(){
        return this.incorrect;
    }

}
